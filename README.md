# Wordpress Docker Containers
This repo contains the configuration for Wordpress application development docker containers.

## Preinstallation
##### Clone the repo
```
git clone git@bitbucket.org:rccz/wordpressdock.git
```
##### Change .env variable values as your new application needs
The repo is prepared to run the initial configuration, only the specific values must be changed:

* WEB_PORT
* WORDPRESS_DB_HOST
* WORDPRESS_DB_NAME
* WORDPRESS_DB_USER
* WORDPRESS_DB_PASSWORD
* WORDPRESS_TABLE_PREFIX

Wordpress optional variables

* WORDPRESS_AUTH_KEY
* WORDPRESS_SECURE_AUTH_KEY
* WORDPRESS_LOGGED_IN_KEY
* WORDPRESS_NONCE_KEY
* WORDPRESS_AUTH_SALT
* WORDPRESS_SECURE_AUTH_SALT
* WORDPRESS_LOGGED_IN_SALT
* WORDPRESS_NONCE_SALT
* WORDPRESS_DEBUG (0 or 1)

Multisite variables

* WP_ALLOW_MULTISITE
* MULTISITE
* SUBDOMAIN_INSTALL
* DOMAIN_CURRENT_SITE
* PATH_CURRENT_SITE
* SITE_ID_CURRENT_SITE
* BLOG_ID_CURRENT_SITE

Mysql variables

* MYSQL_DATABASE
* MYSQL_USER
* MYSQL_PASSWORD
* MYSQL_RANDOM_ROOT_PASSWORD

## Installation
```
docker-compose up -d
```
###### *Permissions issues*
*If you can't edit files, run the following command:*
```
sudo chmod a+rwx -R src/ && sudo chmod a+rwx -R data/
```
###### Credits
Thanks to original docker hub wordpress
> [Oficial Wordpress](https://hub.docker.com/_/wordpress)